This is currently building for the OpenWRT 19.07.7 release

The set of files to use in a OpenWRT custom feed can be retrieve from [here](https://gitlab.freedesktop.org/wicadmin/openwrt-modemmanager-packages/-/jobs/artifacts/master/download?job=package) or via terminal with:

```
wget -O OpenWRT-ModemManager.zip https://gitlab.freedesktop.org/wicadmin/openwrt-modemmanager-packages/-/jobs/artifacts/master/download?job=package
```
These files are built nightly and should therefore be the most up to date.


Special thanks to [Jeffery To](https://github.com/jefferyto) for the work on [Vivarium](https://github.com/jefferyto/openwrt-vivarium) and to [freedesktop.org](https://www.freedesktop.org) for this Gitlab. Of course many thanks to the developers and maintainers of ModemManager and all its supporting libraries for [mobile-broadband](https://gitlab.freedesktop.org/mobile-broadband).
